import math
import numpy as np
import os
import cv2
import Ogre
import Ogre.Bites
import Ogre.RTShader

PATH = os.path.join(os.path.dirname(__file__), 'models')

MARKER_SIZE = 5.3
TRANSLATION = (0.5 * MARKER_SIZE, 0.5 * MARKER_SIZE, 0.0)
TRANSLATION = (0.0, 0.0, 0.0)
SCALE_CANDLE = (3, 3, 3)
SCALE_PYRAMID = (1.5, 1.5, 1.5)
SCALE_TOWER = (3, 3, 3)

# calibration_time: "Sun 26 Feb 2023 12:37:06 PM CET"
#   image_width: 640
#   image_height: 480
#   flags: 0
#   camera_matrix: !!opencv-matrix
#      rows: 3
#      cols: 3
#      dt: d
#      data: [ 6.7623836924305317e+02, 0., 3.1515464058226985e+02, 0.,
#          6.7533365465872964e+02, 2.6205252490254821e+02, 0., 0., 1. ]
#   distortion_coefficients: !!opencv-matrix
#      rows: 1
#      cols: 5
#      dt: d
#      data: [ 3.2595539640997873e-01, -1.4800209855287687e+00,
#          -1.3269859052393138e-03, 1.5746685318079165e-03,
#          2.0488028302332109e+00 ]
#   avg_reprojection_error: 9.2530112395520880e-01
IMAGE_WIDTH = 640
IMAGE_HEIGHT = 480
CAMERA_MATRIX = np.array([6.7623836924305317e+02, 0., 3.1515464058226985e+02, 0.,
         6.7533365465872964e+02, 2.6205252490254821e+02, 0., 0., 1.]).reshape(3, 3)
DISTORTION_COEFFS = np.array([3.2595539640997873e-01, -1.4800209855287687e+00,
         -1.3269859052393138e-03, 1.5746685318079165e-03,
         2.0488028302332109e+00 ])



def set_camera_intrinsics(cam, K, imsize):
    cam.setAspectRatio(imsize[0]/imsize[1])

    zNear = cam.getNearClipDistance()
    top = zNear * K[1, 2] / K[1, 1]
    left = -zNear * K[0, 2] / K[0, 0]
    right = zNear * (imsize[0] - K[0, 2]) / K[0, 0]
    bottom = -zNear * (imsize[1] - K[1, 2]) / K[1, 1]

    cam.setFrustumExtents(left, right, top, bottom)

    fovy = math.atan2(K[1, 2], K[1, 1]) + math.atan2(imsize[1] - K[1, 2], K[1, 1])
    cam.setFOVy(fovy)

def create_image_background(scn_mgr):
    tex = Ogre.TextureManager.getSingleton().create("bgtex", Ogre.RGN_DEFAULT, True)
    tex.setNumMipmaps(0)

    mat = Ogre.MaterialManager.getSingleton().create("bgmat", Ogre.RGN_DEFAULT)
    mat.getTechnique(0).getPass(0).createTextureUnitState().setTexture(tex)
    mat.getTechnique(0).getPass(0).setDepthWriteEnabled(False)
    mat.getTechnique(0).getPass(0).setLightingEnabled(False)

    rect = scn_mgr.createScreenSpaceRect(True)
    rect.setMaterial(mat)
    rect.setRenderQueueGroup(Ogre.RENDER_QUEUE_BACKGROUND)
    scn_mgr.getRootSceneNode().attachObject(rect)

    return tex

def main(ctx):
    # image size
    imsize = (IMAGE_WIDTH, IMAGE_HEIGHT)

    # setup Ogre for AR
    scn_mgr = ctx.getRoot().createSceneManager()
    Ogre.RTShader.ShaderGenerator.getSingleton().addSceneManager(scn_mgr)

    # camera
    cam = scn_mgr.createCamera("camera")
    cam.setNearClipDistance(5)
    ctx.getRenderWindow().setFullscreen(True, 1920, 1080)
    ctx.getRenderWindow().addViewport(cam)
    camnode = scn_mgr.getRootSceneNode().createChildSceneNode()
    camnode.rotate((1, 0, 0), math.pi)  # convert OpenCV to OGRE coordinate system
    camnode.attachObject(cam)

    set_camera_intrinsics(cam, CAMERA_MATRIX, imsize)
    bgtex = create_image_background(scn_mgr)

    # setup 3D scene
    scn_mgr.setAmbientLight((.1, .1, .1))
    scn_mgr.getRootSceneNode().createChildSceneNode().attachObject(scn_mgr.createLight())
    marker_node = scn_mgr.getRootSceneNode().createChildSceneNode()

    # objects folder
    Ogre.ResourceGroupManager.getSingleton().addResourceLocation(
      PATH, "FileSystem");

    # candle AR
    node_candle_A = marker_node.createChildSceneNode()
    node_candle_A.attachObject(scn_mgr.createEntity("candle_code_A.gltf"))
    node_candle_A.rotate((1, 0, 0), math.pi/2)
    node_candle_A.translate(TRANSLATION)
    node_candle_A.scale(SCALE_CANDLE)
    node_candle_A.setVisible(False)
    node_candle_R = marker_node.createChildSceneNode()
    node_candle_R.attachObject(scn_mgr.createEntity("candle_code_R.gltf"))
    node_candle_R.rotate((1, 0, 0), math.pi/2)
    node_candle_R.translate(TRANSLATION)
    node_candle_R.scale(SCALE_CANDLE)
    node_candle_R.setVisible(False)

    # pyramid CJ
    node_pyramid_C = marker_node.createChildSceneNode()
    node_pyramid_C.attachObject(scn_mgr.createEntity("pyramid_code_C.gltf"))
    node_pyramid_C.rotate((1, 0, 0), math.pi/2)
    node_pyramid_C.rotate((0, 1, 0), math.pi/2)
    node_pyramid_C.translate((0, 0, 0))
    node_pyramid_C.scale(SCALE_PYRAMID)
    node_pyramid_C.setVisible(False)
    node_pyramid_J = marker_node.createChildSceneNode()
    node_pyramid_J.attachObject(scn_mgr.createEntity("pyramid_code_J.gltf"))
    node_pyramid_J.rotate((1, 0, 0), math.pi/2)
    node_pyramid_J.rotate((0, 1, 0), math.pi/2)
    node_pyramid_J.translate((0, 0, 0))
    node_pyramid_J.scale(SCALE_PYRAMID)
    node_pyramid_J.setVisible(False)

    # tower BS
    node_tower_B = marker_node.createChildSceneNode()
    node_tower_B.attachObject(scn_mgr.createEntity("tower_code_B.gltf"))
    node_tower_B.rotate((1, 0, 0), math.pi/2)
    node_tower_B.translate(TRANSLATION)
    node_tower_B.scale(SCALE_TOWER)
    node_tower_B.setVisible(False)
    node_tower_S = marker_node.createChildSceneNode()
    node_tower_S.attachObject(scn_mgr.createEntity("tower_code_S.gltf"))
    node_tower_S.rotate((1, 0, 0), math.pi/2)
    node_tower_S.translate(TRANSLATION)
    node_tower_S.scale(SCALE_TOWER)
    node_tower_S.setVisible(False)

    # map marker ids to objects
    objects = {
        0: node_candle_A,
        1: node_candle_R,
        2: node_pyramid_C,
        3: node_pyramid_J,
        4: node_tower_B,
        5: node_tower_S,
    }

    # aruco
    adict = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_50)

    # video capture
    cap = cv2.VideoCapture(2)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, imsize[0])
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, imsize[1])
    while cv2.waitKey(1) != 27:
        # get camera image
        cont, img = cap.read()
        if not cont:
            # exit if camera fails
            break

        # opencv image to ogre
        im = Ogre.Image(Ogre.PF_BYTE_BGR, img.shape[1], img.shape[0], 1, img, False)
        if bgtex.getBuffer():
            bgtex.getBuffer().blitFromMemory(im.getPixelBox())
        else:
            bgtex.loadImage(im)

        corners, ids = cv2.aruco.detectMarkers(img, adict)[:2]

        if ids is not None:
            print(ids)
            marker = ids[0][0]
            if marker in objects:
                # estimate camera position
                rvecs, tvecs = cv2.aruco.estimatePoseSingleMarkers(corners, MARKER_SIZE, CAMERA_MATRIX, DISTORTION_COEFFS)[:2]
                rvec, tvec = rvecs[0].ravel(), tvecs[0].ravel()
                # set world position
                ax = Ogre.Vector3(*rvec)
                ang = ax.normalise()
                marker_node.setOrientation(Ogre.Quaternion(ang, ax))
                marker_node.setPosition(tvec)
                # set current object visible
                objects[marker].setVisible(True)
            # hide all other objects
            for obj_id in objects:
                if obj_id != marker:
                    objects[obj_id].setVisible(False)
        else:
            # no detection => hide all objects
            for key in objects:
                objects[key].setVisible(False)

        # render
        ctx.getRoot().renderOneFrame()


# main
ctx = Ogre.Bites.ApplicationContext()
ctx.initApp()
main(ctx)
ctx.closeApp()
