# ARUCO PUZZLE

Code to setup a puzzle using ARUCO markers (dictionary "ARTOOLKITPLUS").

The solution is on the `imagemaps/` folder. Edit freely the images to set your own solution which is:

    122   129
    182   234

This are also the markers that you need to print.

Also edit the success image with whatever image you want to show on success.

## Requirements

This code has been tested to work with:

* OpenCV >= 3.2.0
* ARUCO >= 3.1.8
