#include <iostream>
#include <map>
#include <opencv2/aruco.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <vector>

// keep global variables here
namespace
{
// debug mode
const bool DEBUG = true;
// solution images (puzzle index => image)
std::map<int, cv::Mat> SOLUTION_IMAGES;
// max accepted distance in pixels between marker corners to be correct
const double SOLUTION_UNCERTAINTY_PIXELS = 60.0f;
// images to overlap (id => image)
std::map<int, cv::Mat> IMAGES;
// unrecognized marker
cv::Mat IMAGE_UNKNOWN;
cv::Scalar COLOR_UNKNOWN = { 255, 255, 0 };
// image fail
cv::Mat IMAGE_FAIL;
cv::Scalar COLOR_FAIL = { 0, 0, 255 };
int ID_FAIL = 200;
// homography of all images (300x300 pixels)
const std::vector<cv::Point2f> HOMOGRAPHY_SRC = { { 0, 0 }, { 300, 0 }, { 300, 300 }, { 0, 300 } };
// color for each marker (sequentially red, green, blue, violet)
const std::vector<cv::Scalar> COLORS = { { 0, 0, 255 }, { 0, 255, 0 }, { 255, 0, 0 }, { 255, 0, 255 } };

/**
 * \brief Structure to represent a puzzle solution.
 */
struct Puzzle
{
  int index;                  //!< index of the puzzle solution
  std::array<int, 4> pieces;  //!< pieces of solution by rows [0 1]; [2 3]

  /**
   * \brief Get the index of the marker id queried.
   * \param mid Marker id to query
   * \return Id of the marker or '-1' if not found
   */
  int get_marker_index(const int mid) const
  {
    for (size_t i = 0; i < pieces.size(); ++i)
    {
      if (pieces[i] == mid)
      {
        return i;
      }
    }
    return -1;
  }
};
// ARTOOLKITPLUS definitions (ARTOOLKITPLUS_id => NEW_id in custom dict)
const std::
    vector<std::vector<int>>
        ARTOOLKITPLUS =
            {
              { 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1,
                1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0 },  // 0
                                                                         // =>
                                                                         // 0
              { 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0,
                1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0 },  // 1
                                                                         // =>
                                                                         // 1
              { 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1,
                1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1 },  // 2
                                                                         // =>
                                                                         // 2
              { 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1,
                1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1 },  // 10
                                                                         // =>
                                                                         // 3
              { 0, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1,
                1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 },  // 50
                                                                         // =>
                                                                         // 4
              { 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1,
                1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1 },  // 100
                                                                         // =>
                                                                         // 5
              { 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1,
                1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0 },  // 122
                                                                         // =>
                                                                         // 6
              { 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1,
                0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0 },  // 129
                                                                         // =>
                                                                         // 7
              { 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1,
                1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1 },  // 150
                                                                         // =>
                                                                         // 8
              { 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0 },  // 182
                                                                         // =>
                                                                         // 9
              { 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1,
                1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1 },  // 200
                                                                         // =>
                                                                         // 10
              { 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1,
                1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0 },  // 234
                                                                         // =>
                                                                         // 11
              { 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1,
                1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1 },  // 250
                                                                         // =>
                                                                         // 12

            };
// Defined puzzles to solve
const std::vector<Puzzle> PUZZLES = { { 0, { 6, 7, 9, 11 } }, { 1, { 1, 2, 3, 4 } }, { 2, { 5, 8, 10, 12 } } };
// ids and corresponding images to load
const std::map<int, std::string> ID2FILE = {
  // puzzle 0
  { PUZZLES.at(0).pieces.at(0), "p0_0.png" },
  { PUZZLES.at(0).pieces.at(1), "p0_1.png" },
  { PUZZLES.at(0).pieces.at(2), "p0_2.png" },
  { PUZZLES.at(0).pieces.at(3), "p0_3.png" },
  // puzzle 1
  { PUZZLES.at(1).pieces.at(0), "p1_0.png" },
  { PUZZLES.at(1).pieces.at(1), "p1_1.png" },
  { PUZZLES.at(1).pieces.at(2), "p1_2.png" },
  { PUZZLES.at(1).pieces.at(3), "p1_3.png" },
  // puzzle 2
  { PUZZLES.at(2).pieces.at(0), "p2_0.png" },
  { PUZZLES.at(2).pieces.at(1), "p2_1.png" },
  { PUZZLES.at(2).pieces.at(2), "p2_2.png" },
  { PUZZLES.at(2).pieces.at(3), "p2_3.png" },
};
}  // namespace

cv::Mat generateMarkerBits(const std::vector<int> &marker)
{
  cv::Mat bits = cv::Mat(6, 6, CV_8UC1);
  for (int i = 0; i < 6; i++)
  {
    for (int j = 0; j < 6; j++)
    {
      const int idx = i * 6 + j;
      bits.at<uchar>(i, j) = marker[idx];
    }
  }
  return bits;
}

cv::aruco::Dictionary get_artoolkitplus_dict()
{
  cv::aruco::Dictionary dictionary;
  // markers of 6x6 bits
  dictionary.markerSize = 6;
  // maximum number of bit corrections
  dictionary.maxCorrectionBits = 3;
  // lets create a dictionary
  for (const auto &marker : ARTOOLKITPLUS)
  {
    // assume generateMarkerBits() generates a new marker in binary format, so that
    // markerBits is a 6x6 matrix of CV_8UC1 type, only containing 0s and 1s
    const cv::Mat markerBits = generateMarkerBits(marker);
    const cv::Mat markerCompressed = cv::aruco::Dictionary::getByteListFromBits(markerBits);
    // add the marker as a new row
    dictionary.bytesList.push_back(markerCompressed);
  }
  return dictionary;
}

/**
 * \brief Check if marker positions correspond to solution.
 * \param markers Detected markers on input image.
 * \param corners If solution is correct, get the outer corners of the 2x2 markers puzzle.
 * \return Index of the correct puzzle or -1 otherwise.
 */
int checkSolution(const std::vector<int> &m_ids, const std::vector<std::vector<cv::Point2f>> &m_corners,
                  std::vector<cv::Point2f> &corners);

/**
 * \brief Show images instead of detected markers.
 * \param image Input image from camera.
 * \param markers Detected markers on input image.
 * \return Input image with images on detected markers.
 */
cv::Mat showImagesOnMarkers(const cv::Mat &image, const std::vector<int> &m_ids,
                            const std::vector<std::vector<cv::Point2f>> &m_corners);

/**
 * \brief Wrap an image according to the detected corners of a marker into another image.
 * \param marker Detected marker with corners
 * \param marker_img Image to wrap according to corners of marker
 * \param inputoutput_img Image where to add the wrapped marker image
 */
void wrapMarker(const std::vector<cv::Point2f> &corners, const cv::Mat &marker_img, cv::Mat &inputoutput_img);

/**
 * \brief Main function controlling execution.
 */
int main(int argc, char **argv)
{
  // check
  if (argc != 3)
  {
    std::cerr << "Usage:\n  " << argv[0] << " camera_index path_to_images\n";
    std::cerr << "  example:  " << argv[0] << " 2 ../imagemaps/" << std::endl;
    return EXIT_FAILURE;
  }

  // path without final '/'
  std::string path = argv[2];
  if (path.at(path.size() - 1) == '/')
  {
    path = path.substr(0, path.size() - 1);
  }

  // load images from aruco marker ids
  char temp[400];
  for (auto &puzzle : PUZZLES)
  {
    // load individual images
    for (auto &aid : puzzle.pieces)
    {
      if (IMAGES.count(aid) == 0)
      {
        std::snprintf(temp, sizeof(temp), "%s/%s", path.c_str(), ID2FILE.at(aid).c_str());
        std::cout << "load: " << temp << '\n';
        IMAGES[aid] = cv::imread(temp);
        assert((IMAGES[aid].rows == 300) && (IMAGES[aid].cols == 300));
      }
    }
    // load solution image
    std::snprintf(temp, sizeof(temp), "%s/success_%d.png", path.c_str(), puzzle.index);
    std::cout << "load: " << temp << '\n';
    SOLUTION_IMAGES[puzzle.index] = cv::imread(temp);
    assert((SOLUTION_IMAGES[puzzle.index].rows == 300) && (SOLUTION_IMAGES[puzzle.index].cols == 300));
  }
  // load unknown marker image
  std::snprintf(temp, sizeof(temp), "%s/unknown.png", path.c_str());
  std::cout << "load: " << temp << '\n';
  IMAGE_UNKNOWN = cv::imread(temp);
  assert((IMAGE_UNKNOWN.rows == 300) && (IMAGE_UNKNOWN.cols == 300));

  // load fail marker image
  std::snprintf(temp, sizeof(temp), "%s/fail.png", path.c_str());
  std::cout << "load: " << temp << '\n';
  IMAGE_FAIL = cv::imread(temp);
  assert((IMAGE_FAIL.rows == 300) && (IMAGE_FAIL.cols == 300));

  // open camera
  cv::VideoCapture capture(std::atoi(argv[1]));

  // aruco detector
  const cv::aruco::Dictionary dict = get_artoolkitplus_dict();
  const cv::Ptr<cv::aruco::Dictionary> dictionary = cv::makePtr<cv::aruco::Dictionary>(dict);

  // keep reading images
  cv::namedWindow("image", cv::WINDOW_NORMAL);
  cv::resizeWindow("image", 1366, 768);
  cv::setWindowProperty("image", cv::WND_PROP_FULLSCREEN, cv::WINDOW_FULLSCREEN);
  cv::Mat image;
  while (true)
  {
    // get image
    capture.read(image);
    cv::Mat output = image.clone();
    // detect markers
    std::vector<int> m_ids;
    std::vector<std::vector<cv::Point2f>> m_corners;
    cv::aruco::detectMarkers(image, dictionary, m_corners, m_ids);
    // check solution
    std::vector<cv::Point2f> global_corners;
    const int pindex = checkSolution(m_ids, m_corners, global_corners);
    if (pindex >= 0)
    {
      if (pindex == ID_FAIL)
      {
        wrapMarker(global_corners, IMAGE_FAIL, output);
      }
      else
      {
        // fake marker as solution "marker" to wrap image
        wrapMarker(global_corners, SOLUTION_IMAGES[pindex], output);
      }
    }
    else
    {
      // show images on markers
      output = showImagesOnMarkers(image, m_ids, m_corners);
    }
    // show image
    cv::imshow("image", output);
    cv::waitKey(10);
  }
}

bool distanceCheck(const cv::Point2f &p0, const cv::Point2f &p1, const double &threshold)
{
  // distance
  const double dist = cv::norm(p0 - p1);
  // debug info
  if (DEBUG)
  {
    std::cout.setf(std::ios::fixed, std::ios::floatfield);
    std::cout.precision(6);
    std::cout << dist << ' ';
  }
  // condition
  return (dist < threshold);
}

int checkSolution(const std::vector<int> &m_ids, const std::vector<std::vector<cv::Point2f>> &m_corners,
                  std::vector<cv::Point2f> &global_corners)
{
  // fast check
  if (m_ids.size() != 4)
  {
    return -1;
  }
  // fast dictionary for queries
  std::map<int, std::vector<cv::Point2f>> mdict;
  for (size_t i = 0; i < m_ids.size(); ++i)
  {
    mdict[m_ids[i]] = m_corners[i];
  }
  // for each puzzle
  for (auto &puzzle : PUZZLES)
  {
    // check all pieces present
    size_t count = 0;
    for (const auto m_id : m_ids)
    {
      if (puzzle.get_marker_index(m_id) >= 0)
      {
        ++count;
      }
    }
    if (count != puzzle.pieces.size())
    {
      continue;  // this puzzle is not valid for sure
    }
    // debug info
    if (DEBUG)
    {
      std::cout << "seems puzzle " << puzzle.index << '\n';
    }

    // check pieces position
    // solution is by rows [0 1]; [2 3]
    const int p0 = puzzle.pieces.at(0);
    const int p1 = puzzle.pieces.at(1);
    const int p2 = puzzle.pieces.at(2);
    const int p3 = puzzle.pieces.at(3);
    bool ok = true;
    // check neighboring corners of markers to be within distance
    ok &= distanceCheck(mdict[p0][1], mdict[p1][0], SOLUTION_UNCERTAINTY_PIXELS);        // top center
    ok &= distanceCheck(mdict[p1][2], mdict[p3][1], SOLUTION_UNCERTAINTY_PIXELS);        // middle right
    ok &= distanceCheck(mdict[p3][3], mdict[p2][2], SOLUTION_UNCERTAINTY_PIXELS);        // bottom center
    ok &= distanceCheck(mdict[p2][0], mdict[p0][3], SOLUTION_UNCERTAINTY_PIXELS);        // middle left
    ok &= distanceCheck(mdict[p0][2], mdict[p1][3], 1.5 * SOLUTION_UNCERTAINTY_PIXELS);  // center p0 p1
    ok &= distanceCheck(mdict[p0][2], mdict[p3][0], 1.5 * SOLUTION_UNCERTAINTY_PIXELS);  // center p0 p2
    ok &= distanceCheck(mdict[p0][2], mdict[p2][1], 1.5 * SOLUTION_UNCERTAINTY_PIXELS);  // center p0 p3
    ok &= distanceCheck(mdict[p1][3], mdict[p3][0], 1.5 * SOLUTION_UNCERTAINTY_PIXELS);  // center p1 p2
    ok &= distanceCheck(mdict[p1][3], mdict[p2][1], 1.5 * SOLUTION_UNCERTAINTY_PIXELS);  // center p1 p3
    ok &= distanceCheck(mdict[p3][0], mdict[p2][1], 1.5 * SOLUTION_UNCERTAINTY_PIXELS);  // center p2 p3
    if (DEBUG)
    {
      std::cout << '\n';
    }
    // return
    if (ok)
    {
      if (DEBUG)
      {
        std::cout << "ok: " << puzzle.index << '\n';
      }
      global_corners.resize(4);
      global_corners[0] = mdict[p0][0];
      global_corners[1] = mdict[p1][1];
      global_corners[2] = mdict[p3][2];
      global_corners[3] = mdict[p2][3];
      return puzzle.index;
    }
  }
  // four markers and bad pieces positioning but close enough to be a completed puzzle
  bool ok = true;
  std::vector<double> distances;
  // first marker against other 3
  for (size_t m2 = 1; m2 < 4; m2++)
  {
    // check all corners against all
    double mindist = 1e100;
    for (const auto &c1 : m_corners[0])
    {
      for (const auto &c2 : m_corners[m2])
      {
        const double dist = cv::norm(c1 - c2);
        if (dist < mindist)
        {
          mindist = dist;
        }
      }
    }
    // distance below max (diagonal)
    if (mindist < 1.5 * SOLUTION_UNCERTAINTY_PIXELS)
    {
      distances.push_back(mindist);
    }
    else
    {
      ok = false;
      break;
    }
  }
  // all 3 distances are below 1.5*TH => 2 must be below TH
  if (ok)
  {
    int count = 0;
    for (const auto &dist : distances)
    {
      if (dist < SOLUTION_UNCERTAINTY_PIXELS)
      {
        count++;
      }
    }
    if (count >= 2)
    {
      // compute global corners
      global_corners.clear();
      // TODO: hardcoded image 480x640
      std::vector<cv::Point2f> image_corners = { { 0, 0 }, { 640, 0 }, { 640, 480 }, { 0, 480 } };
      for (const auto &image_c : image_corners)
      {
        cv::Point2f pt;
        float mindist = 1e100;
        for (const auto &marker : m_corners)
        {
          for (const auto &corner : marker)
          {
            const float dist = cv::norm(image_c - corner);
            if (dist < mindist)
            {
              mindist = dist;
              pt = corner;
            }
          }
        }
        global_corners.push_back(pt);
      }
      return ID_FAIL;
    }
  }

  // otherwise
  return -1;
}

void wrapMarker(const std::vector<cv::Point2f> &corners, const cv::Mat &marker_img, cv::Mat &inputoutput_img)
{
  // init
  static std::vector<cv::Point2f> pts_dst(4);
  // homography points of marker
  for (int i = 0; i < 4; ++i)
  {
    pts_dst[i] = corners[i];
  }
  // compute homography
  const cv::Mat h = cv::findHomography(HOMOGRAPHY_SRC, pts_dst);
  cv::Mat wrapped_image;
  warpPerspective(marker_img, wrapped_image, h, inputoutput_img.size());
  // find a mask where the wrapped image is
  cv::Mat no_image_mask;
  cv::inRange(wrapped_image, cv::Scalar(0, 0, 0), cv::Scalar(0, 0, 0), no_image_mask);
  // set original image to black where wrapped_image is
  inputoutput_img.setTo(cv::Scalar(0, 0, 0), no_image_mask == 0);
  // add both images
  inputoutput_img += wrapped_image;
}

cv::Mat showImagesOnMarkers(const cv::Mat &image, const std::vector<int> &m_ids,
                            const std::vector<std::vector<cv::Point2f>> &m_corners)
{
  // init
  static std::vector<cv::Point2f> pts_dst(4);
  cv::Mat output = image.clone();
  // for each marker show its image
  for (size_t i = 0; i < m_ids.size(); ++i)
  {
    const int m_id = m_ids[i];
    bool found = false;
    cv::Scalar color = COLOR_UNKNOWN;
    // look at all puzzles
    for (auto &puzzle : PUZZLES)
    {
      // get marker index in the puzzle
      const int index = puzzle.get_marker_index(m_id);
      if (index >= 0)
      {
        color = COLORS[index];
        found = true;
      }
      else
      {
        continue;  // nothing else to do
      }
    }  // for puzzles in puzzles

    // get marker image
    cv::Mat img = IMAGE_UNKNOWN;
    if (IMAGES.count(m_id) > 0)
    {
      img = IMAGES[m_id];
    }
    else
    {
      std::cout << "marker: " << m_id << " image not loaded\n";
    }
    wrapMarker(m_corners[i], img, output);

    // color around marker
    // for (int j = 0; j < 4; ++j)
    // {
    //   cv::line(output, m_corners[i][j], m_corners[i][(j + 1) % 4], color, 1);
    // }

    // show corners
    // cv::circle(output, marker[0], 10, cv::Scalar(0, 0, 255), -1);
    // cv::circle(output, marker[1], 10, cv::Scalar(0, 255, 0), -1);
    // cv::circle(output, marker[2], 10, cv::Scalar(255, 0, 0), -1);
    // cv::circle(output, marker[3], 10, cv::Scalar(255, 0, 255), -1);

    // not found in any puzzle
    if (!found)
    {
      std::cout << "marker: " << m_id << " not part of any puzzle\n";
    }
  }  // for marker in markers
  // return
  return output;
}
